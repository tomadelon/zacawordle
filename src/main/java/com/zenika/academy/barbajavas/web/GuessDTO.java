package com.zenika.academy.barbajavas.web;

import com.fasterxml.jackson.annotation.JsonCreator;

public class GuessDTO {

    public String guess;

    @JsonCreator
    public GuessDTO(String guess) {
        this.guess = guess;
    }

    public String getGuess() {
        return guess;
    }

    public void setGuess(String guess) {
        this.guess = guess;
    }
}
