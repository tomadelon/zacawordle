package com.zenika.academy.barbajavas.web;


import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.configuration.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.configuration.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.configuration.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.configuration.domain.service.IllegalWordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GameController {

    private GameManager gameManager;

    @Autowired
    public GameController(GameManager gameManager) {
        this.gameManager = gameManager;
    }

   // @PostMapping
    //public ResponseEntity<Game> gameStart(@RequestParam Integer wordLength,@RequestParam Integer nbAttempts) {

    //    return ResponseEntity.ok(gameManager.startNewGame(wordLength, nbAttempts));
    //}


  @PostMapping("/games")
    public Game createGame(@RequestParam(value= "wordLength", defaultValue = "5") int wordLength, @RequestParam(value = "nbAttempts", defaultValue = "6")int nbAttempts) {
      return gameManager.startNewGame(wordLength, nbAttempts);
  }

  @PostMapping("/games/{gameTid}")
    public Game postWord(@PathVariable String gameTid, @RequestBody GuessDTO guess) throws BadLengthException, IllegalWordException {
      return gameManager.attempt(gameTid, guess.getGuess());
  }

 @GetMapping("/games/{gameTid}")
    public Game gameState(@PathVariable String gameTid){
        return gameManager.getGameTid(gameTid);
  }

}
