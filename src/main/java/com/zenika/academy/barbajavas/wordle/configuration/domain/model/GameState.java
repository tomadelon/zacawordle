package com.zenika.academy.barbajavas.wordle.configuration.domain.model;

public enum GameState {
    WIN, LOSS, IN_PROGRESS
}
