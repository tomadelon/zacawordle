package com.zenika.academy.barbajavas.wordle.configuration.domain.service.displayer;

import com.zenika.academy.barbajavas.wordle.configuration.domain.model.RoundResult;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public interface Displayer {
    default String format(List<RoundResult> results, boolean withLetter) {
        return results.stream()
                .map(r -> this.format(r, withLetter))
                .collect(Collectors.joining(System.lineSeparator()));
    }
    
    String format(RoundResult result, boolean withLetter);
}
