package com.zenika.academy.barbajavas.wordle.configuration.domain.service;

import org.springframework.stereotype.Component;

@Component
public interface DictionaryService {
    String getRandomWord(int length);
    boolean wordExists(String word);
}
