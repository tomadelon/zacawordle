package com.zenika.academy.barbajavas.wordle.configuration.domain.model;

public enum ValidationLetter {
    GOOD_POSITION,
    WRONG_POSITION,
    NOT_IN_WORD
}
