package com.zenika.academy.barbajavas.wordle.configuration;

import com.zenika.academy.barbajavas.wordle.configuration.domain.service.displayer.console.color.ConsoleColorDisplayer;
import com.zenika.academy.barbajavas.wordle.configuration.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.configuration.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration
public class BeanWordleConfiguration {
   @Bean
    I18n i18n(@Value ("${wordle.language}")String language) throws Exception {
       return I18nFactory.getI18n(language);
   }

   @Bean
    Scanner scanner(){
       return new Scanner(System.in);
   }







}
