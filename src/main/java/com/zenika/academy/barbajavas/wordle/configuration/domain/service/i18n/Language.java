package com.zenika.academy.barbajavas.wordle.configuration.domain.service.i18n;

public enum Language {
    FR,
    EN;
}
